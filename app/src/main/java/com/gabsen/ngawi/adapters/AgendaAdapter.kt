package com.gabsen.ngawi.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.AgendaDataModel
import java.util.ArrayList

class AgendaAdapter(private val context: Context?, listAgenda: ArrayList<AgendaDataModel>) : RecyclerView.Adapter<AgendaAdapter.Holder>() {

    private val items = listAgenda

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_agenda, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: AgendaAdapter.Holder, position: Int) {
        val data_agenda: AgendaDataModel = items[position]
        holder.txtLokasi.text = data_agenda.lokasi
        holder.txtNama.text = data_agenda.nama
        holder.txtTanggal.text = data_agenda.tanggal
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var txtNama: TextView = view.findViewById(R.id.agenda_nama) as TextView
        internal var txtLokasi: TextView = view.findViewById(R.id.agenda_lokasi) as TextView
        internal var txtTanggal: TextView = view.findViewById(R.id.agenda_tanggal) as TextView
    }
}