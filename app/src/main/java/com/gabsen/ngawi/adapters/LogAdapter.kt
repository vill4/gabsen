package com.gabsen.ngawi.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.LogModel

import java.util.ArrayList

/**
 * Created by ASUS-PC on 06/10/2017.
 */

class LogAdapter(private val context: Context?, listRiwayat: ArrayList<LogModel>) : RecyclerView.Adapter<LogAdapter.Holder>() {

    private val items = listRiwayat

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_riwayat, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val logNow = items[position]
        if (position > 0) {
            val logBefore = items[position - 1]
            if (logNow.tgl == logBefore.tgl) {
                holder.txtTgl.visibility = View.GONE
            } else {
                holder.txtTgl.visibility = View.VISIBLE
            }
        } else {
            holder.txtTgl.visibility = View.VISIBLE
        }
        val cd:ImageView = holder.imageView.findViewById(R.id.item_log_image)
        if(logNow.jenis == "Check In"){
            cd.setImageResource(R.drawable.ic_fingerprint_green_24dp)
        }else{
            cd.setImageResource(R.drawable.ic_fingerprint_red_24dp)
        }
        holder.txtTgl.text = logNow.tgl
        holder.txtKeterangan.text = logNow.keterangan
    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var imageView: ImageView = view.findViewById<View>(R.id.item_log_image) as ImageView
        internal var txtKeterangan: TextView = view.findViewById<View>(R.id.item_log_keterangan) as TextView
        internal var txtTgl: TextView = view.findViewById<View>(R.id.item_log_date) as TextView
    }
}
