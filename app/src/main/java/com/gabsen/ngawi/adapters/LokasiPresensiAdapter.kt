package com.gabsen.ngawi.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.LokasiPresensiModel
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap

@Suppress("NAME_SHADOWING")
class LokasiPresensiAdapter(private val context: Context, private val mapboxMap: MapboxMap, private val listLokasi: ArrayList<LokasiPresensiModel>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.row_item_lokasi, parent, false)

        val nama = rowView.findViewById<TextView>(R.id.nama_lokasi)

        val lokasi = getItem(position) as LokasiPresensiModel
        nama.text = lokasi.nama

        nama.setOnClickListener {
            val latlong = LatLng()
            latlong.latitude = listLokasi.get(position).lat.toDouble()
            latlong.longitude = listLokasi.get(position).long.toDouble()
            val position: CameraPosition = CameraPosition.Builder()
                    .target(LatLng(latlong.latitude, latlong.longitude))
                    .zoom(17.0)
                    .build()
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 3000)
        }

        return rowView
    }

    override fun getItem(position: Int): Any {
        return listLokasi.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listLokasi.size
    }

}