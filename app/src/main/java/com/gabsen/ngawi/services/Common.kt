package com.gabsen.ngawi.services

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.gabsen.ngawi.activities.LoginActivity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.*


class Common {

    fun logout(code: Int, context: Context): Boolean {
        if (code == 401) {
            var sp = SessionManager(context)
            sp?.clearSharedPreference()
            Toast.makeText(context, "UnAuthorization User", Toast.LENGTH_LONG).show()
            var intlgn = Intent(context, LoginActivity::class.java)
            context.startActivity(intlgn)
            return false
        } else {
            return true
        }
    }

    public fun encodeImage(path: String): String {
        val imagefile = File(path)
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(imagefile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        fis?.close()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun decodeImage(base64: String, path: String) {
        val fileImg:File = File(path)
        val fos = FileOutputStream(fileImg)
        fos.write(Base64.decode(base64, Base64.NO_WRAP))
        fos.close()
    }
}