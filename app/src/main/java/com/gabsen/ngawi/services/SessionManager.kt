package com.gabsen.ngawi.services

import android.content.Context
import android.content.SharedPreferences

public class SessionManager(val context: Context) {

    private val PREFS_NAME = "absenSM"
    val sharedPref:SharedPreferences = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text:String){
        val editor: SharedPreferences.Editor = sharedPref.edit();
        editor.putString(KEY_NAME,text).commit()
    }

    fun save(KEY_NAME: String, value:Int){
        val editor: SharedPreferences.Editor = sharedPref.edit();
        editor.putInt(KEY_NAME,value).commit()
    }

    fun save(KEY_NAME: String, status:Boolean){
        val editor: SharedPreferences.Editor = sharedPref.edit();
        editor.putBoolean(KEY_NAME,status).commit()
    }

    fun getValueString(KEY_NAME: String): String? {
        return sharedPref.getString(KEY_NAME,null)
    }

    fun getValueInt(KEY_NAME: String): Int? {
        return sharedPref.getInt(KEY_NAME,0)
    }

    fun getValueBoolean(KEY_NAME: String): Boolean?{
        return sharedPref.getBoolean(KEY_NAME,false)
    }

    fun clearSharedPreference(){
        var edt: SharedPreferences.Editor = sharedPref.edit();

        edt.clear();
        edt.commit();
    }

    fun removeKey(KEY_NAME: String){
        var edt: SharedPreferences.Editor = sharedPref.edit();

        edt.remove(KEY_NAME)
        edt.commit()
    }
}