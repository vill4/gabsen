package com.gabsen.ngawi.services

import com.gabsen.ngawi.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor



object DataRepository {

    fun create(): Service {
        val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        val baseUrl: String = "http://absen.geomedia.co.id/api/"
//        val baseUrl: String = "http://192.168.1.2/geomedia_absen/api/"

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            httpClient.addInterceptor(logging)
        }
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl).client(httpClient.build())
                .build()
        return retrofit.create(Service::class.java)
    }
}