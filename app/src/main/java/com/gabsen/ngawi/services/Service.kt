package com.gabsen.ngawi.services

import com.gabsen.ngawi.models.*
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @GET("login")
    fun getLogin(@Query("usrname") username:String, @Query("psword")password:String): Call<LoginModel>

    @FormUrlEncoded
    @POST("ubahpassword")
    fun saveUbahPass(@Header("Authorization") authorization:String, @Field("id") id:Int,
                     @Field("oldpwd") oldpwd:String, @Field("newpwd") newpwd:String,
                     @Field("renewpwd") renewpwd:String): Call<Response>

    @GET("history")
    fun getLogabsen(@Header("Authorization") authorization:String, @Query("id") id:Int,
                    @Query("limit") limit:Int, @Query("offset") offset:Int,
                    @Query("tgl_start") start:String, @Query("tgl_end") end: String): Call<LogModelFetch>

    @GET("cekabsentoday")
    fun cekAbsen(@Header("Authorization") authorization:String, @Query("id") id:Int): Call<HomeModel>

    @FormUrlEncoded
    @POST("presensi")
    fun presensi(@Header("Authorization") authorization:String, @Field("id") id:Int,
                     @Field("timestamp") tmstamp:String, @Field("latitude") latitude:Double,
                     @Field("longitude") longitude:Double, @Field("status") status:Int): Call<HomeModel>

    @GET("gettimelokasi")
    fun getTimeLokasi(@Header("Authorization") authorization:String, @Query("id") id:Int, @Query("latitude") latitude:Double,
                      @Query("longitude") longitude:Double): Call<AbsenModel>

    @GET("cart")
    fun getCart(@Header("Authorization") authorization: String, @Query("id") id: Int): Call<ChartModel>

    @GET("listagenda")
    fun getAgenda(@Header("Authorization") authorization: String, @Query("id") id: Int): Call<AgendaModel>

    @GET("cekrekam")
    fun isRekam(@Header("Authorization") authorization: String, @Query("id") id: Int): Call<Response>

    @GET("lokasiabsen")
    fun getLokasiAbsen(@Header("Authorization") authorization: String, @Query("id") id: Int): Call<LokasiFetchModel>

    @FormUrlEncoded
    @POST("facescanupload")
    fun uploadScanFace(@Header("Authorization") authorization: String, @Field("id") id: Int, @Field("img") imgBase64Encode:String ): Call<Response>
}
