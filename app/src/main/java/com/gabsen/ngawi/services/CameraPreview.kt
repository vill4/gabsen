package com.gabsen.ngawi.services

import android.annotation.SuppressLint
import android.hardware.Camera
import android.content.Context
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import java.io.IOException

@SuppressLint("ViewConstructor")
class CameraPreview(context: Context, camera: Camera) : SurfaceView(context), SurfaceHolder.Callback {

    private var mCamera: Camera
    private val surfaceHolder: SurfaceHolder

    init {
        this.mCamera = camera
        this.surfaceHolder = getHolder()
        this.surfaceHolder.addCallback(this)
        this.surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        try {
            this.mCamera.setPreviewDisplay(holder)
            this.mCamera.startPreview()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        refreshCamera(mCamera)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        this.mCamera.stopPreview()
        this.mCamera.release()
    }

    fun refreshCamera(camera: Camera) {
        if (surfaceHolder.surface == null) {
            return
        }

        try {
            camera.stopPreview()
        } catch (e: Exception) {

        }

        setCamera(camera)
        try {
            mCamera.setPreviewDisplay(surfaceHolder)
            mCamera.startPreview()
        } catch (e: Exception) {
            Log.d(View.VIEW_LOG_TAG, "Error starting camera preview: " + e.message)
        }
    }

    private fun setCamera(camera: Camera) {
        this.mCamera = camera
    }
}