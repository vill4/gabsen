package com.gabsen.ngawi.services

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

class ManagePermisions(val activity: Activity, val code: Int) {

    private final val TAG: String = "ManagePermission"
    val listPermission: List<String> = listOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    fun checkPermission() {
        this.requestPermissions()
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(activity, listPermission.toTypedArray(), code)
    }

    // Process permissions result
    fun processPermissionsResult(requestCode: Int, grantResults: IntArray): Boolean {
        var result = 0
        if (grantResults.isNotEmpty()) {
            for (item in grantResults) {
                result += item
            }
        }
        if (result == PackageManager.PERMISSION_GRANTED) return true
        return false
    }
}