package com.gabsen.ngawi.fragments


import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.Manifest.permission
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.ProgressBar

import com.gabsen.ngawi.R
import com.gabsen.ngawi.activities.AbsenActivity
import com.gabsen.ngawi.activities.LoginActivity
import com.gabsen.ngawi.adapters.AgendaAdapter
import com.gabsen.ngawi.models.*
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.SessionManager
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment : Fragment() {

    internal var datang: Boolean? = true
    internal var id = Locale("in", "ID")
    internal var PERMISSION_FINE_LOCATION: Int = 1
    internal var PERMISSION_COURSE_LOCATION: Int = 1
    private var sp: SessionManager? = null
    private var call: Call<HomeModel>? = null
    private var callChart: Call<ChartModel>? = null
    private var callAgenda: Call<AgendaModel>? = null
    val services = DataRepository.create()
    lateinit var agendaAdp: AgendaAdapter
    internal var itemsAgenda = ArrayList<AgendaDataModel>()
    internal val REQUEST_CODE_ABSEN:Int = 1
    private lateinit var VIEW_PARENT:View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.hide()
        val btnCheckIn = view.findViewById(R.id.btn_check_in) as Button
        val btnCheckOut = view.findViewById(R.id.btn_check_out) as Button
        val refresLayout: SwipeRefreshLayout = view.findViewById(R.id.root_layout) as SwipeRefreshLayout
        VIEW_PARENT = view

        if (!cekPermission(view)) {
            return
        }

        refresLayout.setOnRefreshListener {
            cekAbsenToday(view)
            refresLayout.isRefreshing = false
        }

        btnCheckIn.setOnClickListener {
            val intent = Intent(context, AbsenActivity::class.java)
            intent.putExtra("setcekin", datang)
            startActivityForResult(intent,REQUEST_CODE_ABSEN)
        }

        btnCheckOut.setOnClickListener {
            val intent = Intent(context, AbsenActivity::class.java)
            intent.putExtra("setcekin", datang)
            startActivityForResult(intent,REQUEST_CODE_ABSEN)
        }

        agendaAdp = AgendaAdapter(context, itemsAgenda)
        val recyclerView:RecyclerView = view.findViewById(R.id.recycler_agenda) as RecyclerView
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = agendaAdp
        cekAbsenToday(view)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_ABSEN && resultCode == Activity.RESULT_OK) {
             cekAbsenToday(VIEW_PARENT)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroyView() {
        call?.cancel()
        callChart?.cancel()
        callAgenda?.cancel()
        super.onDestroyView()
    }

    private fun cekAbsenToday(view: View) {
        sp = SessionManager(view.context)

        if (sp?.getValueString("SP_TOKEN") == null) {
            sp?.clearSharedPreference()
            val intent = Intent(context, LoginActivity::class.java)
            context?.startActivity(intent)
            return
        }

        val token: String = sp?.getValueString("SP_TOKEN")!!
        val id: Int = sp?.getValueString("SP_ID")!!.toInt()
        val nama: String = sp?.getValueString("SP_NAMA")!!
        val nip: String = sp?.getValueString("SP_NIP")!!
        val lbl_nama: TextView = view.findViewById(R.id.label_nama) as TextView
        val lbl_nip: TextView = view.findViewById(R.id.label_nip) as TextView
        val lbl_tanggal: TextView = view.findViewById(R.id.dash_text_tanggal) as TextView
        lbl_nama.text = nama
        lbl_nip.text = nip
        call = services.cekAbsen(token, id)
        call!!.enqueue(object : Callback<HomeModel> {
            override fun onResponse(call: Call<HomeModel>, response: Response<HomeModel>) {
                val bd = response.body()
                val btnCheckIn = view.findViewById(R.id.btn_check_in) as Button
                val btnCheckOut = view.findViewById(R.id.btn_check_out) as Button
                val lbl_check_status: TextView = view.findViewById(R.id.dash_text_status) as TextView
                val lbl_check_desc: TextView = view.findViewById(R.id.dash_text_check_description) as TextView
                if (bd != null && response.isSuccessful) {
                    val cmn = Common()
                    if (cmn.logout(bd.code, requireContext())) {
                        if (!bd.status) {
                            btnCheckIn.isEnabled = false
                            btnCheckOut.isEnabled = false
                            lbl_check_status.text = "Hari Libur"
                            lbl_check_desc.text = bd.message
                        } else {
                            lbl_tanggal.text = bd.data.tanggal
                            if (bd.data.jam_masuk == null && bd.data.jam_pulang == null) {
                                btnCheckIn.visibility = View.VISIBLE
                                btnCheckOut.visibility = View.GONE
                                datang = true
                                lbl_check_status.text = "Absen Masuk"
                                lbl_check_desc.text = "Anda belum melakukan presensi"
                            } else {
                                // if (bd.data.jam_masuk != null && bd.data.jam_pulang == null)
                                val pulang_detail: List<String> = getTime(bd.data.jadwal_pulang)
                                lbl_check_status.text = "Absen Pulang"
                                lbl_check_desc.text = "Jam pulang pukul : " + pulang_detail.get(1)
                                btnCheckIn.visibility = View.GONE
                                btnCheckOut.visibility = View.VISIBLE
                                datang = false
                            }
//                        else {
//                            lbl_check_status.text = "Absen Pulang"
//                            lbl_check_desc.text = ""
//                            btnCheckIn.isEnabled = false
//                            btnCheckOut.isEnabled = false
//                            datang = false
//                        }
                        }
                    } else {
                        activity!!.finish()
                    }
                }
            }

            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e("Error", "Onfailur CekAbsenToday : " + t.message)
            }
        })

        callChart = services.getCart(token, id)
        val pcHadir: PieChart = view.findViewById(R.id.chart_absen) as PieChart
        val pcOnTime: PieChart = view.findViewById(R.id.chart_ontime) as PieChart
        val progres_hadir: ProgressBar = view.findViewById(R.id.pgb_chart_absen) as ProgressBar
        val progres_ontime: ProgressBar = view.findViewById(R.id.pgb_chart_ontime) as ProgressBar
        pcHadir.visibility = View.INVISIBLE
        pcOnTime.visibility = View.INVISIBLE
        progres_hadir.visibility = View.VISIBLE
        progres_ontime.visibility = View.VISIBLE
        callChart!!.enqueue(object : Callback<ChartModel> {
            override fun onResponse(call: Call<ChartModel>, response: Response<ChartModel>) {
                val bd: ChartModel? = response.body()
                val btnCheckIn = view.findViewById(R.id.btn_check_in) as Button
                val btnCheckOut = view.findViewById(R.id.btn_check_out) as Button
                if (bd != null && response.isSuccessful) {
                    val cmn = Common()
                    if (cmn.logout(bd.code, requireContext())) {
                        val dtInHadir: List<ChartDataModel> = bd.data.hadir
                        val dtInOntime: List<ChartDataModel> = bd.data.ontime
                        val dtHadir = ArrayList<PieEntry>()
                        for (data in dtInHadir) {
                            dtHadir.add(PieEntry(data.value.toFloat() + 0, data.key))
                        }
                        val dtOntime = ArrayList<PieEntry>()
                        for (data in dtInOntime) {
                            dtOntime.add(PieEntry(data.value.toFloat() + 0, data.key))
                        }
                        var set: PieDataSet = PieDataSet(dtHadir, "")
                        val desc = Description()
                        desc.text = "Kehadiran"
                        pcHadir.description = desc
                        pcHadir.setDrawEntryLabels(false)
                        var clrs = arrayOf(Color.parseColor("#00A413"), Color.parseColor("#EAF714"))
                        set.colors = clrs.toMutableList()
                        var data: PieData = PieData(set)
                        pcHadir.data = data
                        pcHadir.invalidate()
                        set = PieDataSet(dtOntime, "")
                        clrs = arrayOf(Color.parseColor("#2563F7"), Color.parseColor("#BFBFBF"))
                        set.colors = clrs.toMutableList()
                        data = PieData(set)
                        val desc_telat = Description()
                        desc_telat.text = "Telat / PSW"
                        pcOnTime.description = desc_telat
                        pcOnTime.setDrawEntryLabels(false)
                        pcOnTime.data = data
                        pcOnTime.invalidate()
                        pcHadir.visibility = View.VISIBLE
                        pcOnTime.visibility = View.VISIBLE
                        progres_hadir.visibility = View.INVISIBLE
                        progres_ontime.visibility = View.INVISIBLE
                    } else {
                        activity!!.finish()
                    }
                }
            }

            override fun onFailure(call: Call<ChartModel>, t: Throwable) {
                Log.e("Error", "Onfailur Cart : " + t.message)
                progres_hadir.visibility = View.INVISIBLE
                progres_ontime.visibility = View.INVISIBLE
            }
        })
        itemsAgenda.clear()
        callAgenda = services.getAgenda(token, id)
        callAgenda!!.enqueue(object : Callback<AgendaModel> {
            override fun onResponse(call: Call<AgendaModel>, response: Response<AgendaModel>) {
                val bd = response.body()
                val cmn = Common()
                if (bd != null) {
                    if (cmn.logout(bd.code, requireContext())) {
                        for (dt in bd.data) {
                            var m = AgendaDataModel(dt.nama,dt.lokasi,dt.tanggal)
                            itemsAgenda.add(m)
                        }
                        agendaAdp.notifyDataSetChanged()
                    } else {
                        activity!!.finish()
                    }
                }
            }

            override fun onFailure(call: Call<AgendaModel>, t: Throwable) {
                Log.e("Error", "Onfailur getAgenda : " + t.message)
            }

        })
    }

    private fun cekPermission(view: View): Boolean {
        PERMISSION_FINE_LOCATION = ContextCompat.checkSelfPermission(view.context, android.Manifest.permission.ACCESS_FINE_LOCATION)
        PERMISSION_COURSE_LOCATION = ContextCompat.checkSelfPermission(view.context, android.Manifest.permission.ACCESS_COARSE_LOCATION)
        if (PERMISSION_FINE_LOCATION != PackageManager.PERMISSION_GRANTED || PERMISSION_COURSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(context, "Location Permission Not GRANTED YET", Toast.LENGTH_LONG).show()
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), permission.ACCESS_FINE_LOCATION)) {
                return false
            } else {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission.ACCESS_FINE_LOCATION), 0)
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission.ACCESS_COARSE_LOCATION), 0)
                return true
            }
        } else {
            return true
        }
    }

    private fun getTime(timestamp: String): List<String> {
        val return_var: MutableList<String> = ArrayList<String>()
        val split_timestamp = timestamp.split(" ");
        val tgl: String = split_timestamp.get(0).toString()
        val time: String = split_timestamp.get(1).toString().substring(0, 5)
        return_var.add(0, tgl)
        return_var.add(1, time)
        return return_var
    }

}// Required empty public constructor

