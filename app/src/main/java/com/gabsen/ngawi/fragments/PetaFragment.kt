package com.gabsen.ngawi.fragments


import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.mapbox.mapboxsdk.Mapbox

import com.gabsen.ngawi.R
import com.gabsen.ngawi.adapters.LokasiPresensiAdapter
import com.gabsen.ngawi.models.LokasiFetchModel
import com.gabsen.ngawi.models.LokasiPresensiModel
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.Service
import com.gabsen.ngawi.services.SessionManager
import com.google.android.gms.location.LocationServices
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.annotations.PolygonOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.*
import kotlinx.android.synthetic.main.fragment_map.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.cos
import kotlin.math.sin


/**
 * A simple [Fragment] subclass.
 */
class PetaFragment : Fragment(), OnMapReadyCallback, PermissionsListener {

    var TAG:String = "PetaFragment::fragment "
    lateinit var permissionManager: PermissionsManager
    lateinit var mapboxMap: MapboxMap
    var service: Service = DataRepository.create()
    val lokasiModel = ArrayList<LokasiPresensiModel>()
    private var sp: SessionManager? = null
//    val latLangs = ArrayList<List<LatLng>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(activity!!.applicationContext, getString(R.string.mapbox_public_key))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    @SuppressLint("MissingPermission", "InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fab_absenLocation = view.findViewById(R.id.fab_absenLocation) as FloatingActionButton
        val fab_location = view.findViewById(R.id.fab_mylocation) as FloatingActionButton
        val mapView = view.findViewById(R.id.mapView) as MapView

        sp = SessionManager(context!!)
        val token: String = sp!!.getValueString("SP_TOKEN")!!
        val id: Int = sp!!.getValueString("SP_ID")!!.toInt()

        mapView.onCreate(savedInstanceState)

        mapView.getMapAsync(this)

        service.getLokasiAbsen(token,id).enqueue(object : Callback<LokasiFetchModel>{
            override fun onResponse(call: Call<LokasiFetchModel>, response: Response<LokasiFetchModel>) {
                var bd: LokasiFetchModel? = response.body()
                if(bd!!.status) {
                    for (fetch: LokasiPresensiModel in bd!!.data) {
                        val lks = LokasiPresensiModel(fetch.nama, fetch.lat, fetch.long, fetch.radius)
                        lokasiModel.add(lks)
                    }
                }else{
                    Log.e(TAG,bd.message)
                }
            }

            override fun onFailure(call: Call<LokasiFetchModel>, t: Throwable) {
                Toast.makeText(context,"Error loading lokasi : "+t.message, Toast.LENGTH_LONG).show()
            }

        })

        fab_location.setOnClickListener {
            val fusedLoation = LocationServices.getFusedLocationProviderClient(context!!)
            fusedLoation.lastLocation.addOnSuccessListener {
                val position: CameraPosition = CameraPosition.Builder()
                        .target(LatLng(it.latitude, it.longitude))
                        .zoom(17.0)
                        .build()
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 5000)
            }
        }
        fab_absenLocation.setOnClickListener {
            val lokasiAdapter = LokasiPresensiAdapter(context!!, mapboxMap, lokasiModel)
            if (lokasiModel.size > 1) {
                val mBottomSheetDialog = BottomSheetDialog(activity!!)
                val sheetView: View = activity!!.layoutInflater.inflate(R.layout.list_bottom_dialog_peta, null)
                val title = sheetView.findViewById(R.id.bottom_title) as TextView
                val listView = sheetView.findViewById(R.id.bottom_listview) as ListView

                title.text = "Lokasi Absen"
                listView.adapter = lokasiAdapter
                mBottomSheetDialog.setContentView(sheetView)
                mBottomSheetDialog.show()
            } else {
                val latlong = LatLng()
                latlong.latitude = lokasiModel.get(0).lat.toDouble()
                latlong.longitude = lokasiModel.get(0).long.toDouble()
                val position: CameraPosition = CameraPosition.Builder()
                        .target(LatLng(latlong.latitude, latlong.longitude))
                        .zoom(17.0)
                        .build()
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 5000)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap

        mapboxMap.setStyle(Style.MAPBOX_STREETS) {
            enableLocationComponen(it)
            mapboxMap.moveCamera(CameraUpdateFactory.zoomTo(2.0))

            lokasiModel.forEachIndexed { index, latLng ->
                val latLng = LatLng();
                latLng.latitude = lokasiModel.get(index).lat.toDouble()
                latLng.longitude = lokasiModel.get(index).long.toDouble()
                var radius = lokasiModel.get(index).radius.toDouble()

                val polygonOptions = PolygonOptions()
                polygonOptions.strokeColor(Color.parseColor("#00D4EAFF"))
                polygonOptions.fillColor(Color.parseColor("#00FFFFFF"))
                polygonOptions.addAll(getCirclePoints(latLng, radius))
                mapboxMap.addPolygon(polygonOptions);

                mapboxMap.addMarker(MarkerOptions()
                        .position(latLng)
                        .title("Lokasi absen ke-" + lokasiModel[index]))
            }
        }
    }

    @Suppress("DEPRECATION")
    @SuppressLint("MissingPermission")
    private fun enableLocationComponen(loadedMapStyle: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(context)) {
            val locationComponent: LocationComponent = mapboxMap.locationComponent
            locationComponent.activateLocationComponent(context!!, loadedMapStyle)

            locationComponent.isLocationComponentEnabled = true
            locationComponent.setCameraMode(CameraMode.TRACKING)
            locationComponent.zoomWhileTracking(17.0)
            locationComponent.renderMode = RenderMode.COMPASS
        } else {
            permissionManager = PermissionsManager(this)
            permissionManager.requestLocationPermissions(activity)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        Toast.makeText(context, "Permission location needed", Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap.getStyle({
                enableLocationComponen(it)
            })
        } else {
            Toast.makeText(context, "Permission location not granted", Toast.LENGTH_LONG).show()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mapView != null) mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        if (mapView != null) mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mapView != null) mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        if (mapView != null) mapView.onLowMemory()
    }

    companion object Peta {
        fun getCirclePoints(position: LatLng, radius: Double): ArrayList<LatLng> {
            val degreesBetweenPoints = 10
            val numberOfPoints: Int = (Math.floor(360.0 / degreesBetweenPoints)).toInt()
            val distRadians: Double = radius / 6371000.0 // earth radius in meters
            val centerLatRadians: Double = position.getLatitude() * Math.PI / 180;
            val centerLonRadians: Double = position.getLongitude() * Math.PI / 180;
            val polygons = ArrayList<LatLng>(); // array to hold all the points
            for (index in 0 until numberOfPoints) {
                val degrees = (index * degreesBetweenPoints).toDouble()
                val degreeRadians = degrees * Math.PI / 180
                val pointLatRadians = Math.asin((sin(centerLatRadians) * cos(distRadians)
                        + cos(centerLatRadians) * sin(distRadians) * cos(degreeRadians)))
                val pointLonRadians = (centerLonRadians + Math.atan2((sin(degreeRadians)
                        * sin(distRadians) * cos(centerLatRadians)),
                        cos(distRadians) - sin(centerLatRadians) * sin(pointLatRadians)))
                val pointLat = pointLatRadians * 180 / Math.PI
                val pointLon = pointLonRadians * 180 / Math.PI
                val point = LatLng(pointLat, pointLon)
                polygons.add(point)
            }
            // add first point at end to close circle
            polygons.add(polygons.get(0));
            return polygons;
        }
    }
}