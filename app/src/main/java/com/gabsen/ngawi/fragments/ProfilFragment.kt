package com.gabsen.ngawi.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.gabsen.ngawi.activities.PengaturanActivity

import com.gabsen.ngawi.R
import com.gabsen.ngawi.activities.LoginActivity
import com.gabsen.ngawi.activities.UbahPassActivity
import com.gabsen.ngawi.services.SessionManager

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfilFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_profil, container, false)
        val sp = SessionManager(inflater.context)
        val nama = sp.getValueString("SP_NAMA")
        val nip = sp.getValueString("SP_NIP")
        val textProfil : TextView = view.findViewById(R.id.profil_nama)
        val textNip : TextView = view.findViewById(R.id.profil_nip)
        textProfil.text = nama
        textNip.text = nip

        val textUbahPwd :TextView = view.findViewById(R.id.profil_ubah_pass)
        textUbahPwd.setOnClickListener {
            val intent = Intent(context, UbahPassActivity::class.java)
            startActivity(intent)
        }

        val textLogout :TextView = view.findViewById(R.id.profil_logout)
        textLogout.setOnClickListener {
            val session = SessionManager(inflater.context)
            session.clearSharedPreference()
            Toast.makeText(context,"Logout Berhasil",Toast.LENGTH_LONG).show()
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)
        }

        val textSetting :TextView = view.findViewById(R.id.profil_pengaturan)
        textSetting.setOnClickListener {
            val intent = Intent(context, PengaturanActivity::class.java)
            startActivity(intent)
        }
        // Inflate the layout for this fragment
        return view
    }



}
