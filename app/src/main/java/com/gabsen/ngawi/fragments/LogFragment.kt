package com.gabsen.ngawi.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.gabsen.ngawi.R
import com.gabsen.ngawi.adapters.LogAdapter
import com.gabsen.ngawi.models.LogModel
import com.gabsen.ngawi.models.LogModelFetch
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.SessionManager
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class LogFragment : Fragment() {

    internal var items = ArrayList<LogModel>()
    lateinit var adapter: LogAdapter
    internal var offsetData: Int = 0
    var call: Call<LogModelFetch>? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_log, container, false)
        val recyclerView = view.findViewById(R.id.recycler_log) as RecyclerView
        val swipyLayout = view.findViewById(R.id.swiftlayout) as SwipyRefreshLayout
        swipyLayout.setOnRefreshListener(object : SwipyRefreshLayout.OnRefreshListener {
            override fun onRefresh(direction: SwipyRefreshLayoutDirection?) {
                if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    offsetData = populateData(offsetData)
                } else {
                    offsetData = populateData(0)
                }
                swipyLayout.isRefreshing = false
            }

        })
        adapter = LogAdapter(context, items)
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = adapter
        this.offsetData = populateData(0)
        return view
    }

    override fun onDestroyView() {
        call?.cancel()
        super.onDestroyView()
    }

    private fun populateData(offst: Int): Int {
        val logServices = DataRepository.create()
        val sp = SessionManager(this.requireContext())

        val id: Int = sp.getValueString("SP_ID")!!.toInt()
        val token: String = sp.getValueString("SP_TOKEN")!!
        val dtStart: String = ""
        val dtEnd: String = ""
        val lmt = 5;
        if (offst == 0) {
            items.clear();
        }
        call = logServices.getLogabsen(token, id, lmt, offst, dtStart, dtEnd)
        call?.enqueue(object : Callback<LogModelFetch> {
            override fun onResponse(call: Call<LogModelFetch>, response: Response<LogModelFetch>) {
                val bd = response.body()
                val cmn = Common()
                if (bd != null) {
                    if (cmn.logout(bd.code, requireContext())) {
                        for (dt in bd.data) {
                            var m = LogModel("", "Anda " + dt.jenis + " pukul " + dt.waktu + "\nLokasi : " + dt.lokasi, "", dt.tanggal, dt.jenis)
                            items.add(m)
                        }
                        adapter.notifyDataSetChanged()
                    } else {
                        activity!!.finish()
                    }
                }
            }

            override fun onFailure(call: Call<LogModelFetch>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
        return offst + lmt
    }


}// Required empty public constructor