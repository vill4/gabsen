package com.gabsen.ngawi.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gabsen.ngawi.R
import android.hardware.Camera
import android.util.Log
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import com.gabsen.ngawi.services.CameraPreview
import com.gabsen.ngawi.util.CameraView
import com.gabsen.ngawi.util.CtrRecognizer
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.*
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream



class CameraScanActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {
    private var mCamera: Camera? = null
    private var mPreview: CameraPreview? = null
    private val mPicture: Camera.PictureCallback? = null
    private val capture: Button? = null
    private val switchCamera: Button? = null
    private val myContext: Context? = null
    private var cameraPreview: CameraView? = null
    private val cameraFront = false
//    var mBitmap: Bitmap? = null

    private val mDetectorType = JAVA_DETECTOR

    internal var mPath = ""
    private var mRgba: Mat? = null
    private var mGray: Mat? = null
    private var mCascadeFile: File? = null
    private var mJavaDetector: CascadeClassifier? = null
    private lateinit var fr: CtrRecognizer
    private val mRelativeFaceSize = 0.2f
    private var mAbsoluteFaceSize = 0
    private var mLikely = 999

    private var mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
                when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i(TAG, "OpenCV loaded successfully")
                    val s = "Training..."
                    Toast.makeText(applicationContext, s, Toast.LENGTH_LONG).show()

                    try {
                        // load cascade file from application resources
                        val inpStream:InputStream = resources.openRawResource(R.raw.haarcascade_frontalface_default)
                        val cascadeDir:File = getDir("cascade", Context.MODE_PRIVATE)
                        mCascadeFile = File(cascadeDir, "frontalface.xml")
                        val os = FileOutputStream(mCascadeFile)

                        inpStream.use { input->
                            os.use {fileout->
                                input.copyTo(fileout)
                            }
                        }

                        inpStream.close()
                        os.close()

                        mJavaDetector = CascadeClassifier(mCascadeFile!!.absolutePath)
                        if (mJavaDetector!!.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier")
                            mJavaDetector = null
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile!!.absolutePath)
                        cascadeDir.delete()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Log.e(TAG, "Failed to load cascade. Exception thrown: $e")
                    }
                    cameraPreview!!.enableView()
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_scan)
        setSupportActionBar(mainToolbar)

        supportActionBar?.title = "Rekam Wajah"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        cameraPreview = findViewById(R.id.scanPreview)
        cameraPreview!!.setCvCameraViewListener(this)
        fr = CtrRecognizer()

        cameraPreview!!.setCamFront()
    }

    override fun onPause() {
        super.onPause()
        if(cameraPreview != null)
            cameraPreview!!.disableView()
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Init OpenCV");
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback)
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        mGray = Mat()
        mRgba = Mat();
        countImages = 0;
        countInitFace = 0;
        Log.d(TAG, "Camera Started")
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
        mRgba = inputFrame?.rgba()
        mGray = inputFrame?.gray()

        if (mAbsoluteFaceSize == 0) {
            val height = mGray!!.rows()
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize)
            }
        }

        val faces = MatOfRect()
//        var mRgbaT = mRgba?.t();
//        var mGrayT = mGray?.t();
//        Core.flip(mRgba?.t(), mRgbaT, -1);
//        Imgproc.resize(mRgbaT, mRgba, mRgba?.size());
//
//        Core.flip(mGray?.t(), mGrayT, -1);
//        Imgproc.resize(mGrayT, mGray, mGray?.size());
//        Core.flip(mRgbaT, mRgba, -1)
//        Core.flip(mGrayT, mGray, -1)

        if (mDetectorType == JAVA_DETECTOR) {
            if (mJavaDetector != null)
                mJavaDetector!!.detectMultiScale(mGray!!, faces, 1.1, 3, 2,
                        Size(mAbsoluteFaceSize.toDouble(), mAbsoluteFaceSize.toDouble()), Size())
        } else {
            Log.e(TAG, "Detection method is not selected!")
        }
        val facesArray = faces.toArray()
        if (facesArray.size == 1 && countImages < MAXIMG) {
            val m: Mat
            val r = facesArray[0]

            m = mRgba!!.submat(r)
//            mBitmap = Bitmap.createBitmap(m.height(), m.width(), Bitmap.Config.ARGB_8888)
//            Utils.matToBitmap(m, mBitmap)
            if (countImages < MAXIMG && countInitFace > 5) {
                fr.add(m, "scanlog")
                countImages++
            }
        }else{
            Log.d(TAG,"Face not found, counted :  "+countImages)
            if(countImages >= MAXIMG) {
                Log.d(TAG,"Scan Roll Wajah Berhasil")
                val resultIntent = Intent()
                resultIntent.putExtra(RESULT_SCAN,true)
                setResult(Activity.RESULT_OK, resultIntent)
                finish()
            }
        }
        var tl:Point = Point()
        var br:Point = Point()
        for(i:Int in facesArray.indices) {
//            tl.x = facesArray[i].tl().y
//            tl.y = facesArray[i].tl().x
//            br.x = facesArray[i].br().y
//            br.y = facesArray[i].br().x
            tl  = facesArray[i].tl()
            br  = facesArray[i].br()
            Imgproc.rectangle(mRgba!!, tl, br, FACE_RECT_COLOR, 3)
        }
        countInitFace++
//        mRgbaT?.release()
//        mGrayT?.release()
        return mRgba!!
    }

    override fun onCameraViewStopped() {
        mGray!!.release()
        mRgba!!.release()
        Log.d(TAG, "Camera Stopped")
    }

    companion object {

        private val TAG = "CameraScanActivity::Activity"
        private val FACE_RECT_COLOR = Scalar(0.0, 255.0, 0.0, 255.0)
        private var countImages:Int = 0
        private var countInitFace:Int = 0
        val JAVA_DETECTOR = 0
        val RESULT_SCAN:String = "result_scan"
        internal val MAXIMG: Long = 10
    }
}