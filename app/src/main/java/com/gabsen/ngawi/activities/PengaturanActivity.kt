package com.gabsen.ngawi.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.Response
import com.gabsen.ngawi.models.dataFaceMap
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.Service
import com.gabsen.ngawi.services.SessionManager
import com.gabsen.ngawi.util.CtrRecognizer
import kotlinx.android.synthetic.main.activity_pengaturan.*
import kotlinx.android.synthetic.main.activity_pengaturan.mainToolbar
import retrofit2.Call
import retrofit2.Callback
import kotlin.system.measureTimeMillis
import com.google.gson.GsonBuilder


class PengaturanActivity : AppCompatActivity() {

    var REQUEST_CODE_SCAN: Int = 1
    var REQUEST_CODE_TES: Int = 2
    var fr: CtrRecognizer? = null
    var services: Service = DataRepository.create()
    private var sp: SessionManager? = null
    val thisContext = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.gabsen.ngawi.R.layout.activity_pengaturan)
        setSupportActionBar(mainToolbar)

        supportActionBar?.title = "Pengaturan"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setMenuVisibility()

        setting_rekam_wajah.setOnClickListener {
            val intent = Intent(this, CameraScanActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_SCAN)
        }
        tes_rekam_wajah.setOnClickListener {
            val intent = Intent(this, ScanLogActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_TES)
        }
        delete_rekam_wajah.setOnClickListener {
            sp = SessionManager(this)
            val token: String = sp!!.getValueString("SP_TOKEN")!!
            val id: Int = sp!!.getValueString("SP_ID")!!.toInt()
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Konfirmasi ")
            builder.setMessage("Apakah anda akan menghapus data rekam wajah ?")
            builder.setPositiveButton("Ya") { _, _ ->
                spin_kit.visibility = View.VISIBLE
                services.isRekam(token,id).enqueue(object:Callback<Response>{
                    override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                        var bd:Response? = response.body()
                        if(bd!!.status){
                            fr!!.deleteRekamWajah()
                            Toast.makeText(thisContext, "Rekam Wajah dihapus", Toast.LENGTH_LONG).show()
                            setMenuVisibility()
                        }else{
                            val bld = AlertDialog.Builder(thisContext)
                            bld.setTitle("Pemberitahuan")
                            bld.setMessage(bd.message)
                            bld.setNeutralButton("Tutup",{_,_->})
                            bld.create()
                            bld.show()
                        }
                        spin_kit.visibility = View.INVISIBLE
                    }

                    override fun onFailure(call: Call<Response>, t: Throwable) {
                        Log.e(TAG,"error : "+t.message)
                        Toast.makeText(thisContext, t.message, Toast.LENGTH_LONG).show()
                        spin_kit.visibility = View.INVISIBLE
                    }

                })
            }
            builder.setNegativeButton("Tidak") { _, _-> }
            val dialog = builder.create()
            dialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            val resultScan: Boolean? = data?.getBooleanExtra(CameraScanActivity.RESULT_SCAN, false)
            if (resultScan!!) {
                Toast.makeText(this, "Rekam Wajah Berhasil.", Toast.LENGTH_SHORT).show()
                uploadFace()
            } else {
                Toast.makeText(this, "Rekam Wajah Gagal.", Toast.LENGTH_SHORT).show()
            }
            setMenuVisibility()
        } else if (requestCode == REQUEST_CODE_TES && resultCode == Activity.RESULT_OK) {
            val resultScan: Boolean? = data?.getBooleanExtra(ScanLogActivity.RESULT_SCAN, false)
            if (resultScan!!) {
                Toast.makeText(this, "Scan Berhasil.", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Scan Tidak Berhasil.", Toast.LENGTH_LONG).show()
            }
        } else {
            Log.d(TAG, "Tidak ada result scan : " + requestCode + " : " + resultCode + " / " + data?.getBooleanExtra(CameraScanActivity.RESULT_SCAN, true))
            Log.d(TAG, "Result OK: " + Activity.RESULT_OK + " : " + CameraScanActivity.RESULT_SCAN)
        }


    }

    private fun uploadFace(){
        sp = SessionManager(this)
        val token: String = sp!!.getValueString("SP_TOKEN")!!
        val id: Int = sp!!.getValueString("SP_ID")!!.toInt()
        spin_kit.visibility = View.VISIBLE
        val cmn = Common()
        val imageFiles = fr!!.listImageScan()
        var listImg = ArrayList<String>()
        for (img in imageFiles){
            val base64 = cmn.encodeImage(img.absolutePath)
            listImg.add(base64)
        }
        val gson = GsonBuilder().setPrettyPrinting().create()
        val strBaseImg:String = gson.toJson(listImg)
        Log.d(TAG,"Upload data Scan Wajah")
        Toast.makeText(thisContext, "Upload Data Rekam Wajah", Toast.LENGTH_LONG).show()
        services.uploadScanFace(token,id,strBaseImg).enqueue(object:Callback<Response>{
            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                var bd:Response? = response.body()
                if(bd!!.status){
                    Toast.makeText(thisContext, bd.message, Toast.LENGTH_SHORT).show()
                }else{
                    val bld = AlertDialog.Builder(thisContext)
                    bld.setTitle("Pemberitahuan")
                    bld.setMessage(bd.message)
                    bld.setNeutralButton("Tutup",{_,_->})
                    bld.create()
                    bld.show()
                }
                spin_kit.visibility = View.GONE
            }

            override fun onFailure(call: Call<Response>, t: Throwable) {
                Log.e(TAG,"error : "+t.message)
                Toast.makeText(thisContext, t.message, Toast.LENGTH_LONG).show()
                spin_kit.visibility = View.GONE
            }
        })
    }
    private fun setMenuVisibility() {
        val timeElapsed = measureTimeMillis {
            fr = CtrRecognizer()
        }
        Log.d(TAG,"$timeElapsed")
//        fr = CtrRecognizer()
        var cekFile = fr!!.cekDataRekamWajah()
        if (cekFile) {
            delete_rekam_wajah.visibility = View.VISIBLE
            setting_rekam_wajah.visibility = View.GONE
            layout_tes.visibility = View.VISIBLE
        } else {
            delete_rekam_wajah.visibility = View.GONE
            setting_rekam_wajah.visibility = View.VISIBLE
            layout_tes.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        val TAG: String = "PengaturanActivity::"
    }

}
