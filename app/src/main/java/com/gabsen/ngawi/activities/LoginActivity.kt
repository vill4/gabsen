package com.gabsen.ngawi.activities

import android.app.AlertDialog
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.LoginModel
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.SessionManager
import com.gabsen.ngawi.util.CtrRecognizer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Callback




class LoginActivity : AppCompatActivity() {

    lateinit var animationDrawable: AnimationDrawable
    lateinit var sp: SessionManager
    var thisContext = this
    val TAG = "LoginActivity::"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        supportActionBar?.hide()
        setContentView(R.layout.activity_login)
        sp = SessionManager(applicationContext)
        animationDrawable = login_root_element.background as AnimationDrawable

        animationDrawable.setEnterFadeDuration(5000)
        animationDrawable.setExitFadeDuration(2000)
        cekIslogin()
        text_lupa_pass.setOnClickListener({
            val intent = Intent(this, ForgotPassActivity::class.java)
            startActivity(intent)
//            finish()
        })

        text_syncron_user.setOnClickListener({
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        })
        login_button.setOnClickListener({
            val lgnServices = DataRepository.create()
            val user: String = txt_user.text.toString()
            val pwd: String = txt_pwd.text.toString()

            spin_kit.visibility = View.VISIBLE

            lgnServices.getLogin(user, pwd).enqueue(object : Callback<LoginModel> {
                override fun onResponse(call: retrofit2.Call<LoginModel>, response: retrofit2.Response<LoginModel>) {
                    val bd = response.body()
                    if (bd != null) {
                        if (bd.status) {
                            sp.save("SP_ID", bd.data.id)
                            sp.save("SP_NAMA", bd.data.nama)
//                            sp.save("SP_EMAIL",bd.data.email)
                            sp.save("SP_NIP", bd.data.nip)
                            sp.save("SP_TOKEN", bd.data.token)
                            sp.save("SP_ISLOGIN", true)
                            if (bd.data.facemap != null)
                                decodeImage(bd.data.facemap)

                            val intent = Intent(applicationContext, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            finish()
                        } else {
                            val bld = AlertDialog.Builder(thisContext)
                            bld.setTitle("Pemberitahuan")
                            bld.setMessage(bd.message)
                            bld.setNeutralButton("Tutup",{_,_->})
                            bld.create()
                            bld.show()
                        }
                        spin_kit.visibility = View.GONE
                    }
                }

                override fun onFailure(call: retrofit2.Call<LoginModel>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    spin_kit.visibility = View.GONE
                }

            })
        })
    }


    override fun onPause() {
        super.onPause()

        if (animationDrawable.isRunning) {
            animationDrawable.stop()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!animationDrawable.isRunning) {
            animationDrawable.start()
        }
//        cekIslogin()
    }

    fun cekIslogin(){
        val sp = SessionManager(this)
        Log.d("SP_ISLOGIN", sp.getValueBoolean("SP_ISLOGIN").toString())
        if(sp.getValueBoolean("SP_ISLOGIN") == true){
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    private fun decodeImage(strbase:String){
        var ctr = CtrRecognizer()
        var cmn = Common()
        ctr.deleteRekamWajah()
        val type = object : TypeToken<List<String>>() {}.type
        var gson = GsonBuilder().setPrettyPrinting().create()
        val scanImg:List<String> = gson.fromJson(strbase, type)
        for ((indx, scanImg) in scanImg.withIndex()){
            val pathFile = ctr.getMPath()+"scanlog-"+indx+ctr.extJpg
            cmn.decodeImage(scanImg,pathFile)
        }
    }
}