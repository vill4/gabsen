package com.gabsen.ngawi.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.gabsen.ngawi.R

import com.gabsen.ngawi.fragments.DashboardFragment
import com.gabsen.ngawi.fragments.LogFragment
import com.gabsen.ngawi.fragments.PetaFragment
import com.gabsen.ngawi.fragments.ProfilFragment
import com.gabsen.ngawi.services.ManagePermisions
import com.gabsen.ngawi.services.SessionManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private final var TAG = "MainActivity"
    var savedInstance: Bundle? = null
    var isDashActive: Boolean = false
    var isLogActive: Boolean = false
    var isMapActive: Boolean = false
    var isProfilActive: Boolean = false
    private lateinit var managePermisions: ManagePermisions
    var permissionRequestcode = 123

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        savedInstance = savedInstanceState
        managePermisions = ManagePermisions(this,permissionRequestcode)
        managePermisions.checkPermission()
        setSupportActionBar(mainToolbar)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val sp = SessionManager(this)
        Log.d(TAG, sp.getValueBoolean("SP_ISLOGIN").toString())
        if(sp.getValueBoolean("SP_ISLOGIN") == false){
            val intent = Intent(this, LoginActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
//        Mapbox.getInstance(this, getString(R.string.mapbox_public_key))

        val fragment = DashboardFragment()
        isDashActive = true
        addFragment(fragment, fragment.javaClass.simpleName)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            permissionRequestcode -> {
                val isPermissionsGranted =managePermisions.processPermissionsResult(requestCode,grantResults)
                if (isPermissionsGranted) {
                    Log.i(TAG, "Permission has been granted by user")
                } else {
                    Log.i(TAG, "Permission has been denied by user")
                }
            }
        }
    }
//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.toolbar, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

    @RequiresApi(Build.VERSION_CODES.M)
    @Suppress("DEPRECATION")
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.navigation_dashboard -> {
                if (!isDashActive) {
                    supportActionBar?.hide()

                    isDashActive = true
                    isLogActive = false
                    isMapActive = false
                    isProfilActive = false

                    val fragment = DashboardFragment()
                    addFragment(fragment, fragment.javaClass.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
            }
            R.id.navigation_log -> {
                if (!isLogActive) {
                    supportActionBar?.show()
                    supportActionBar?.title = resources.getString(R.string.title_log)

                    isDashActive = false
                    isLogActive = true
                    isMapActive = false
                    isProfilActive = false

                    val fragment = LogFragment()
                    addFragment(fragment, fragment.javaClass.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
            }
            R.id.navigation_map -> {
                if (!isMapActive) {
                    val fragment = PetaFragment()
                    supportActionBar?.hide()

                    isDashActive = false
                    isLogActive = false
                    isMapActive = true
                    isProfilActive = false

                    addFragment(fragment, "com.mapbox.map")
                    return@OnNavigationItemSelectedListener true
                }
            }
            R.id.navigation_profile -> {
                if (!isProfilActive) {
                    supportActionBar?.show()
                    supportActionBar?.title = resources.getString(R.string.title_profile)

                    isDashActive = false
                    isLogActive = false
                    isMapActive = false
                    isProfilActive = true

                    val fragment = ProfilFragment()
                    addFragment(fragment, fragment.javaClass.simpleName)
                    return@OnNavigationItemSelectedListener true
                }
            }

        }
        false
    }

    private fun addFragment(frag: Fragment, tag: String){
        supportFragmentManager.beginTransaction()
                .replace(R.id.content, frag, tag)
                .commit()
    }

    private var doubleBackToExitPressedOnce = false
    private lateinit var backToast:Toast
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}
