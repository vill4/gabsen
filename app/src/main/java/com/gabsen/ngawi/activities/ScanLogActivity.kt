package com.gabsen.ngawi.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.gabsen.ngawi.R
import com.gabsen.ngawi.util.CameraView
import com.gabsen.ngawi.util.CtrRecognizer
import kotlinx.android.synthetic.main.activity_main.*
import org.bytedeco.javacv.OpenCVFrameConverter
import org.opencv.android.*
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

class ScanLogActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {

    private lateinit var mOpenCvCameraView: CameraView
    private var mRgba: Mat? = null
    private var mGray: Mat? = null
    private final var TAG: String = "ScanLogActivity::"

    private var mCascadeFile: File? = null
    private var mJavaDetector: CascadeClassifier? = null
    private var fr: CtrRecognizer? = null
    private val mRelativeFaceSize = 0.2f
    private var mAbsoluteFaceSize = 0
    var mBitmap: Bitmap? = null
    private val mDetectorType = JAVA_DETECTOR
    private var mLikely = 999
    private var countNotMatch = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_scan_log)
        setSupportActionBar(mainToolbar)

        supportActionBar?.title = "Scan Wajah Absen"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        fr = CtrRecognizer()

        mOpenCvCameraView = findViewById(R.id.surface_view)
        mOpenCvCameraView.setCvCameraViewListener(this)
        mOpenCvCameraView.setCamFront()
    }

    private var mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i(TAG, "OpenCV loaded successfully")
                    try {
                        // load cascade file from application resources
                        val inpStream: InputStream = resources.openRawResource(R.raw.haarcascade_frontalface_default)
                        val cascadeDir:File = getDir("cascade", Context.MODE_PRIVATE)
                        mCascadeFile = File(cascadeDir, "frontalface.xml")
                        val os = FileOutputStream(mCascadeFile)
                        fr!!.load()
                        Log.i(TAG, "Load Train success")
                        inpStream.use { input->
                            os.use {fileout->
                                input.copyTo(fileout)
                            }
                        }

                        inpStream.close()
                        os.close()

                        mJavaDetector = CascadeClassifier(mCascadeFile!!.absolutePath)
                        if (mJavaDetector!!.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier")
                            mJavaDetector = null
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile!!.absolutePath)
                        cascadeDir.delete()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Log.e(TAG, "Failed to load cascade. Exception thrown: $e")
                    }
                    mOpenCvCameraView.enableView()
                }
                else -> {
                    super.onManagerConnected(status)
                    Log.d(TAG,"onManager Not Loading")
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if(mOpenCvCameraView != null)
            mOpenCvCameraView.disableView()
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Init OpenCV");
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback)
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        mRgba = Mat()
        mGray = Mat()
        countNotMatch = 0;
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat? {
        mRgba = inputFrame!!.rgba()
        mGray = inputFrame!!.gray()

        if (mAbsoluteFaceSize == 0) {
            val height = mGray!!.rows()
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize)
            }
        }

        val faces = MatOfRect()
//        var mRgbaT = mRgba?.t();
//        var mGrayT = mGray?.t();
//        Core.flip(mRgbaT, mRgba, -1)
//        Core.flip(mGrayT, mGray, -1)
        if (mDetectorType == JAVA_DETECTOR) {
            if (mJavaDetector != null)
                mJavaDetector!!.detectMultiScale(mGray!!, faces, 1.1, 3, 2,
                        Size(mAbsoluteFaceSize.toDouble(), mAbsoluteFaceSize.toDouble()), Size())
        } else {
            Log.e(TAG, "Detection method is not selected!")
        }
        val facesArray = faces.toArray()
        if (facesArray.size > 0) {
            val m: Mat
            var textTochange:String;
            m = mGray!!.submat(facesArray[0])
//            mBitmap = Bitmap.createBitmap(m.height(), m.width(), Bitmap.Config.ARGB_8888)
//
//            Utils.matToBitmap(m, mBitmap)
            val mm = this.opencvMatToJavacvMat(m)
            if (!mm.isNull) {
                textTochange = fr!!.predict(mm)
                mLikely = fr!!.prob
                Log.d(TAG, "Result : $textTochange, like : "+mLikely+ " >> "+getProb().toString())
                if(getProb()) {
                    val resultIntent = Intent()
                    resultIntent.putExtra(RESULT_SCAN, true)
                    setResult(Activity.RESULT_OK, resultIntent)
                    finish()
                }else{
                    countNotMatch++
                    if(countNotMatch > 30){
                        val resultIntent = Intent()
                        resultIntent.putExtra(RESULT_SCAN, false)
                        setResult(Activity.RESULT_OK, resultIntent)
                        finish()
                    }
                }
            } else {
                Log.d(TAG, "Opencv Mat is NULL : " + m.dataAddr())
            }
        }
        var tl:Point = Point()
        var br:Point = Point()
        for(i:Int in facesArray.indices) {
//            tl.x = facesArray[i].tl().y
//            tl.y = facesArray[i].tl().x
//            br.x = facesArray[i].br().y
//            br.y = facesArray[i].br().x
            tl = facesArray[i].tl()
            br = facesArray[i].br()
            Imgproc.rectangle(mRgba!!, tl, br, FACE_RECT_COLOR, 3)
        }

//        Core.transpose(mRgba,mRgba)
//        Core.flip(mRgba, mRgba, -1)
//        mRgbaT!!.release()
//        mRgbaT!!.release()
        return mRgba!!
    }

    override fun onCameraViewStopped() {
        mRgba!!.release()
        mGray!!.release()
    }

    private fun getProb():Boolean{
        return (mLikely > 0 && mLikely < 85) //recom <80
    }

    companion object {
        val JAVA_DETECTOR = 0
        private val FACE_RECT_COLOR = Scalar(0.0, 255.0, 0.0, 255.0)
        val RESULT_SCAN:String = "result_scan"
        internal val MAXIMG: Long = 10
    }

    private fun opencvMatToJavacvMat(opencvMat: Mat): org.bytedeco.opencv.opencv_core.Mat {
        val convert1 = OpenCVFrameConverter.ToMat()
        val convert2 = OpenCVFrameConverter.ToOrgOpenCvCoreMat()
        return convert1.convert(convert2.convert(opencvMat))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
