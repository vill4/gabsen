package com.gabsen.ngawi.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gabsen.ngawi.R

class ForgotPassActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        supportActionBar?.hide()
        setContentView(R.layout.activity_forgot_pass)
    }
}
