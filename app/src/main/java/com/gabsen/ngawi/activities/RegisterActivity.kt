package com.gabsen.ngawi.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.gabsen.ngawi.R
import kotlinx.android.synthetic.main.activity_register.*
import org.bytedeco.librealsense.context
import android.widget.ArrayAdapter
import android.content.Context
import android.view.ViewGroup
import android.widget.TextView
import android.graphics.Color
import android.view.ViewParent
import android.widget.EditText
import android.widget.Spinner
import java.text.FieldPosition


class RegisterActivity : AppCompatActivity() {

    var non_pns: Boolean = false
    lateinit var opd: MutableSet<MutableMap.MutableEntry<Int, String>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        supportActionBar?.hide()
        setContentView(R.layout.activity_register)

        cb_no_pns.setOnClickListener({
            if (non_pns){
                layout_nip.visibility = View.VISIBLE
                non_pns = false
            } else {
                layout_nip.visibility = View.GONE
                non_pns = true
            }
        })

        register_back.setOnClickListener({
            this.finish()
        })
        listOpd(this)
    }

    fun listOpd(context: Context) {
        val spinnerArray: ArrayList<String> = ArrayList()
        spinnerArray.add("List OPD")
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerArray)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spin_opd.setAdapter(adapter)
    }
}
