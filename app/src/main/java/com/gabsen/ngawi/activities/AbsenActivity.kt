package com.gabsen.ngawi.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.AbsenModel
import com.gabsen.ngawi.models.HomeModel
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.Service
import com.gabsen.ngawi.services.SessionManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_absen.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.mainToolbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class AbsenActivity : AppCompatActivity() {

    val TAG:String = "AbsenActivity::"
    internal var detik = 0
    internal var menit = 0
    internal var jam = 0
    var handler: Handler? = null
    var lblWaktu: TextView? = null
    private var sp: SessionManager? = null
    var isCekin: Boolean = false
    var btnCheckIn: Button? = null
    var btnCheckOut: Button? = null
    var txtHari: TextView? = null
    var txtTanggal: TextView? = null
    var txtLokasi: TextView? = null
    var cal: Calendar? = null
    var services: Service = DataRepository.create()
    final val ctx:Context = this
    private lateinit var mFuseLocation: FusedLocationProviderClient
    var REQUEST_CODE_ABSEN: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absen)
        setSupportActionBar(mainToolbar)

        supportActionBar?.title = "Kehadiran"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnCheckIn = findViewById(R.id.btn_check_in)
        btnCheckOut = findViewById(R.id.btn_check_out)
        txtHari = findViewById(R.id.txt_hari)
        txtTanggal = findViewById(R.id.txt_tanggal)
        txtLokasi = findViewById(R.id.txt_lokasi)
        lblWaktu = findViewById(R.id.txt_waktu)

        val i: Intent = intent
        isCekin = i.getBooleanExtra("setcekin", false)
        countStart()
        btnCheckIn!!.setOnClickListener {
            this.showCekLogDialog("Cek in", "Apakah anda akan melakukan Cek IN ?")
        }
        btnCheckOut!!.setOnClickListener {
            this.showCekLogDialog("Cek Out", "Apakah anda akan melakukan Cek Out ?")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setButtonStatus(iscekin: Boolean) {
        if (iscekin == false) {
            btnCheckIn!!.visibility = View.INVISIBLE
            btnCheckOut!!.visibility = View.VISIBLE
        } else {
            btnCheckIn!!.visibility = View.VISIBLE
            btnCheckOut!!.visibility = View.INVISIBLE
        }
    }


    @SuppressLint("MissingPermission")
    private fun countStart() {
        sp = SessionManager(this)
        val pb: ProgressBar = findViewById(R.id.pgb_waiting)
        pb.visibility = View.VISIBLE
        val token: String = sp!!.getValueString("SP_TOKEN")!!
        val id: Int = sp!!.getValueString("SP_ID")!!.toInt()
        mFuseLocation = LocationServices.getFusedLocationProviderClient(this)
        mFuseLocation.lastLocation.addOnSuccessListener {
            services.getTimeLokasi(token, id, it.latitude, it.longitude).enqueue(object : Callback<AbsenModel> {
                @SuppressLint("SimpleDateFormat")
                override fun onResponse(call: Call<AbsenModel>, response: Response<AbsenModel>) {
                    val bd: AbsenModel? = response.body()
                    cal = Calendar.getInstance()
                    var sdf: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    if (bd != null) {
                        pb.visibility = View.GONE
                        val cmn = Common()
                        var strTgl: String = bd.data.timestamp
                        cal!!.time = sdf.parse(strTgl)
                        if (cmn.logout(bd.code, applicationContext)) {
                            if (!bd.status) {
                                btnCheckIn!!.isEnabled = false
                                btnCheckOut!!.isEnabled = false
                                Toast.makeText(applicationContext, "Status false", Toast.LENGTH_LONG)
                            } else {
                                jam = cal!!.get(Calendar.HOUR_OF_DAY)
                                menit = cal!!.get(Calendar.MINUTE)
                                detik = cal!!.get(Calendar.SECOND)
                                txtHari!!.text = bd.data.hari
                                txtTanggal!!.text = bd.data.tanggal
                                txtLokasi!!.text = bd.data.lokasi
                                handler = Handler()
                                handler?.postDelayed(runnable, 0)
                                setButtonStatus(isCekin)
                            }
                        } else {
                            finish()
                        }
                    } else {
                        Toast.makeText(applicationContext, "No respon accepted. Server Maintenance", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<AbsenModel>, t: Throwable) {
                    Log.e("Error",t.message)
                    pb.visibility = View.GONE
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                }

            })
        }
    }

    var runnable: Runnable = object : Runnable {
        override fun run() {
            detik++
            if (detik > 59) {
                menit++
                detik = 0
            }
            if (menit > 59) {
                jam++
                menit = 0
            }
            if (jam > 23) {
                jam = 0
            }
            val lDetik: String = if (detik <= 9) "0" + detik.toString() else detik.toString()
            val lMenit: String = if (menit <= 9) "0" + menit.toString() else menit.toString()
            val lJam: String = if (jam <= 9) "0" + jam.toString() else jam.toString()
            lblWaktu!!.text = lJam + ":" + lMenit + ":" + lDetik
            handler?.postDelayed(this, 1000)
        }
    }

    @SuppressLint("MissingPermission")
    private fun sendCeklog() {
        val dialog = AlertDialog.Builder(ctx)
        sp = SessionManager(applicationContext)
        val token: String = sp!!.getValueString("SP_TOKEN")!!
        val id: Int = sp!!.getValueString("SP_ID")!!.toInt()
        val time = lblWaktu!!.text.toString()
        val status: Int = if (isCekin) 0 else 1
        mFuseLocation = LocationServices.getFusedLocationProviderClient(applicationContext)
        mFuseLocation.lastLocation.addOnSuccessListener {
            services.presensi(token, id, time, it.latitude, it.longitude, status).enqueue(object : Callback<HomeModel> {
                override fun onResponse(call: Call<HomeModel>, response: Response<HomeModel>) {
                    val bd = response.body()
                    if (bd != null) {
                        val cmn = Common()
                        if (cmn.logout(bd.code, applicationContext)) {
                            if (!bd.status) {
                                dialog.setTitle("Warning")
                                dialog.setMessage(bd.message)
                                dialog.setPositiveButton("Tutup"){_, _->
                                    Log.d("msg_absen",bd.message)
                                }
                                dialog.show()
                            }else{
                                val resultIntent = Intent()
                                setResult(Activity.RESULT_OK, resultIntent)
                                finish()
                            }
                        } else {
                            finish()
                        }
                    }
                }

                override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                    Log.e("error absen ", "Error failure : " + t.message)
                }
            })
        }
    }

    private fun showCekLogDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Konfirmasi " + title)
        builder.setMessage(message)
//        builder.setPositiveButton("Yes") { dialog, which ->
//            sendCeklog(jenis)
//        }
//        builder.setNegativeButton("No") { dialog, which ->
//            Toast.makeText(applicationContext, title + " dibatalkan", Toast.LENGTH_LONG).show()
//        }
//        val dialog = builder.create()
//        dialog.show()
        val intScan = Intent(this, ScanLogActivity::class.java)
        startActivityForResult(intScan,REQUEST_CODE_ABSEN);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_CODE_ABSEN && resultCode == Activity.RESULT_OK){
            val resultScan: Boolean? = data?.getBooleanExtra(ScanLogActivity.RESULT_SCAN, false)
            if (resultScan!!) {
                sendCeklog()
            } else {
                Toast.makeText(this, "Scan Tidak Berhasil.", Toast.LENGTH_LONG).show()
            }
        }
    }

}
