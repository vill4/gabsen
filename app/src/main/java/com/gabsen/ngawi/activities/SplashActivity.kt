package com.gabsen.ngawi.activities

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gabsen.ngawi.R
import android.content.Intent
import android.content.pm.PackageManager
import android.media.audiofx.EnvironmentalReverb
import android.os.Environment
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.gabsen.ngawi.services.ManagePermisions
import com.gabsen.ngawi.util.CtrRecognizer
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

class SplashActivity : AppCompatActivity() {

    lateinit var managePermisions: ManagePermisions
    var permissionRequestcode = 123
    val TAG: String = "SplashActivity::"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionBar?.hide()
        supportActionBar?.hide()
        setContentView(R.layout.splash_screen)
        if (ceknrequestpermission()) {
            runLoadLib()
        }
        Log.d(TAG,filesDir.absolutePath);
    }

    fun ceknrequestpermission(): Boolean {
        val readExternal = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val writeExternal = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        val listPermission = ArrayList<String>()
        if (readExternal != PackageManager.PERMISSION_GRANTED) {
            listPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (writeExternal != PackageManager.PERMISSION_GRANTED) {
            listPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (!listPermission.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermission.toTypedArray(), permissionRequestcode)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            permissionRequestcode -> {
                val perms = HashMap<String, Int>()
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED

                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]

                    if (perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED &&
                            perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED) {
                        runLoadLib()
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            var bld = AlertDialog.Builder(this)
                                    .setMessage("Service Permissions are required for this app")
                                    .setPositiveButton("OK", { dialog, wich ->
                                        ceknrequestpermission()
                                    })
                                    .setNegativeButton("Cancel", { dialog, wich ->
                                        finish()
                                    });
                            val dialog = bld.create()
                            bld.show()
                        }
                    }
                }
            }
        }
    }

    fun runLoadLib() {
        var dirLib:File = File(filesDir.absolutePath+"/lib")
        if(!dirLib.exists()){
//            dirLib.mkdir()
            Log.d(TAG,"Create dir = "+dirLib.absolutePath)
        }else{
            Log.d(TAG,"Dir Exist = "+dirLib.absolutePath)
        }
        var dir:File = filesDir
        var strDir:ArrayList<String> = ArrayList()
        var f = dir.listFiles()
        for(fl in f){
            if(fl.isDirectory){
                Log.d(TAG,fl.name)
            }
        }
        Handler().postDelayed(Runnable {
            val fr = CtrRecognizer()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 1000)

    }
}
