package com.gabsen.ngawi.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.gabsen.ngawi.R
import com.gabsen.ngawi.models.Response
import com.gabsen.ngawi.services.Common
import com.gabsen.ngawi.services.DataRepository
import com.gabsen.ngawi.services.SessionManager
import kotlinx.android.synthetic.main.activity_ubahpwd.*
import retrofit2.Call
import retrofit2.Callback

class UbahPassActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ubahpwd)
        setSupportActionBar(mainToolbar)

        supportActionBar?.title = "Ubah Password"
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_ubahpwd.setOnClickListener {
            val lgnServices = DataRepository.create()
            val sp = SessionManager(applicationContext)
            val old:String = txt_old_pwd.text.toString()
            val new:String = txt_new_pwd.text.toString()
            val renew:String = txt_renew_pwd.text.toString()
            val id:Int = sp.getValueString("SP_ID")!!.toInt()
            val token:String = sp.getValueString("SP_TOKEN")!!

            spin_kit.visibility = View.VISIBLE
            lgnServices.saveUbahPass(token,id,old,new,renew).enqueue(object : Callback<Response>{
                override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                    val bd = response.body();
                    val cmn = Common()
                    if (bd != null) {
                        if (cmn.logout(bd.code, applicationContext)){
                            Toast.makeText(applicationContext,bd.message,Toast.LENGTH_LONG).show()
                        }else{
                            finish();
                        }
                    }
                    spin_kit.visibility = View.GONE
                }
                override fun onFailure(call: Call<Response>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    spin_kit.visibility = View.GONE
                }

            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
