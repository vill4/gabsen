package com.gabsen.ngawi.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.hardware.Camera.PictureCallback
import android.hardware.Camera.Size
import android.util.AttributeSet
import android.util.Log

import org.opencv.android.JavaCameraView

import java.io.FileOutputStream

class CameraView(context: Context, attrs: AttributeSet) : JavaCameraView(context, attrs) {

    val effectList: List<String>
        get() = mCamera.parameters.supportedColorEffects

    val isEffectSupported: Boolean
        get() = mCamera.parameters.colorEffect != null

    var effect: String
        get() = mCamera.parameters.colorEffect
        set(effect) {
            val params = mCamera.parameters
            params.colorEffect = effect
            mCamera.parameters = params
        }

    val resolutionList: List<Size>
        get() = mCamera.parameters.supportedPreviewSizes

    var resolution: Size
        get() = mCamera.parameters.previewSize
        set(resolution) {
            disconnectCamera()
            mMaxHeight = resolution.height
            mMaxWidth = resolution.width
            connectCamera(width, height)
        }

    fun setResolution(w: Int, h: Int) {
        disconnectCamera()
        mMaxHeight = h
        mMaxWidth = w

        connectCamera(width, height)
    }

    fun setAutofocus() {
        val parameters = mCamera.parameters
        parameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO
        mCamera.parameters = parameters
    }

    fun setCamFront() {
        disconnectCamera()
        setCameraIndex(org.opencv.android.CameraBridgeViewBase.CAMERA_ID_FRONT)
        connectCamera(width, height)
    }

    fun setCamBack() {
        disconnectCamera()
        setCameraIndex(org.opencv.android.CameraBridgeViewBase.CAMERA_ID_BACK)
        connectCamera(width, height)
    }

    fun numberCameras(): Int {
        return Camera.getNumberOfCameras()
    }

    fun takePicture(fileName: String) {
        Log.i(TAG, "Tacking picture")
        val callback = PictureCallback { data, camera ->
            Log.i(TAG, "Saving a bitmap to file")
            val picture = BitmapFactory.decodeByteArray(data, 0, data.size)
            try {
                val out = FileOutputStream(fileName)
                picture.compress(Bitmap.CompressFormat.JPEG, 90, out)
                picture.recycle()
                mCamera.startPreview()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        mCamera.takePicture(null, null, callback)
    }

    companion object {
        private val TAG = "Sample::CameraView"
    }
}