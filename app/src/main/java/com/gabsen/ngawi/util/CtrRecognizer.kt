package com.gabsen.ngawi.util

import android.content.ContextWrapper
import android.graphics.Bitmap
import android.os.Environment
import android.util.Log

import org.bytedeco.opencv.global.opencv_imgproc
import org.bytedeco.opencv.opencv_core.IplImage
import org.bytedeco.opencv.opencv_core.Mat
import org.bytedeco.opencv.opencv_core.MatVector
import org.bytedeco.opencv.opencv_face.EigenFaceRecognizer
import org.bytedeco.opencv.opencv_face.FaceRecognizer
import org.bytedeco.opencv.opencv_face.FisherFaceRecognizer
import org.bytedeco.opencv.opencv_face.LBPHFaceRecognizer
import org.opencv.android.Utils

import java.io.File
import java.io.FileOutputStream
import java.io.FilenameFilter
import java.nio.IntBuffer

import org.bytedeco.opencv.global.opencv_core.CV_32SC1
import org.bytedeco.opencv.global.opencv_core.IPL_DEPTH_8U
import org.bytedeco.opencv.global.opencv_imgproc.CV_BGR2GRAY
import org.bytedeco.opencv.global.opencv_imgproc.cvCvtColor
import org.bytedeco.opencv.helper.opencv_imgcodecs.cvLoadImage
import java.util.*


class CtrRecognizer() {
    internal var faceRecognizer: FaceRecognizer
    internal var count = 0
    internal var labelsFile: Labels? = null

    var prob:Int = 999
    val extJpg:String = ".jpg"
    init {
        faceRecognizer = LBPHFaceRecognizer.create(2, 8, 8, 8, 200.0)
        setPath()
    }

    fun setPath(){
        var success = File(mPath).mkdirs()
        if(success)
            Log.d(TAG,"Create "+mPath+" Success")

        labelsFile = Labels(mPath)
        success = File(mPath+".nomedia").createNewFile()
        if(success)
            Log.d(TAG,"Create file .nomedia Success")
    }

    internal fun changeRecognizer(nRec: Int) {
        when (nRec) {
            0 -> faceRecognizer = LBPHFaceRecognizer.create(1, 8, 8, 8, 100.0)
            1 -> faceRecognizer = FisherFaceRecognizer.create()
            2 -> faceRecognizer = EigenFaceRecognizer.create(10, 123.0)
        }
        train()

    }

    internal fun add(m: org.opencv.core.Mat, description: String) {
        var bmp = Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888)

        Utils.matToBitmap(m, bmp)
        bmp = Bitmap.createScaledBitmap(bmp, WIDTH, HEIGHT, false)

        val f: FileOutputStream
        try {
            f = FileOutputStream("$mPath$description-$count"+extJpg, true)
            count++
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, f)
            f.close()

        } catch (e: Exception) {
            Log.e("error", e.cause.toString() + " " + e.message)
            e.printStackTrace()

        }
    }

    fun cekDataRekamWajah():Boolean{
        val root = File(mPath)
        val pngFilter = FilenameFilter { _, name -> name.toLowerCase().endsWith(extJpg) }
        val imageFiles = root.listFiles(pngFilter)
        return imageFiles.size > 0
    }

    fun deleteRekamWajah(){
        val imageFiles = listImageScan()
        for (image:File in imageFiles) {
            if(image.exists() && image.isFile)
                image.delete()
        }
    }

    fun listImageScan():Array<File>{
        val root = File(mPath)
        val pngFilter = FilenameFilter { _, name -> name.toLowerCase().endsWith(extJpg) }
        return root.listFiles(pngFilter)
    }

    fun train(): Boolean {

        val root = File(mPath)

        val pngFilter = FilenameFilter { _, name -> name.toLowerCase().endsWith(extJpg) }
        val imageFiles = root.listFiles(pngFilter)
        val images = MatVector(imageFiles.size.toLong())
        val matlabels = Mat(imageFiles.size, 1, CV_32SC1)
        if (imageFiles.size == 0) {
            return false
        }
        labels = IntArray(imageFiles.size)
        val labelsBuf = matlabels.createBuffer<IntBuffer>()

        var counter = 0

        var img: IplImage? = null
        var grayImg: IplImage

        val i1 = mPath.length

        for (image in imageFiles) {
            val p = image.absolutePath
            val nameFile = image.name
            img = cvLoadImage(p)

            if (img == null)
                Log.e("Error", "Error cVLoadImage")
            Log.i(TAG,"image : " + p)
            Log.i(TAG,"imageName : "+ nameFile)

            val i2 = nameFile.lastIndexOf("-")
            val i3 = nameFile.lastIndexOf(".")
            val icount = Integer.parseInt(nameFile.substring(i2 + 1, i3))
            if (count < icount) count++

            val description = nameFile.substring(0, i2)

            if (labelsFile!!.get(description) < 0) {
                val nMax = labelsFile!!.max() + 1
                labelsFile!!.add(description, nMax)
                Log.d(TAG,"added label : "+description+" : "+nMax)
            }else{
                Log.d(TAG,"Nothing add to label")
            }
            var numlabel = labelsFile!!.get(description)

            grayImg = IplImage.create(img!!.width(), img.height(), IPL_DEPTH_8U, 1)

            cvCvtColor(img, grayImg, CV_BGR2GRAY)
            val matGray = Mat(grayImg)
            images.put(counter.toLong(), matGray)
            labelsBuf.put(counter, icount)

            labels!![counter] = numlabel
            counter++
        }
        Log.d(TAG,"Labelsize : "+ labels)
        if (counter > 0)
            if (labelsFile!!.max() > 0) {
                faceRecognizer.train(images, matlabels)
            }
        labelsFile!!.Save()
        return true
    }

    fun getMPath():String{
        return mPath
    }

//    fun canPredict(): Boolean {
//        return if (labelsFile.max() > 1)
//            true
//        else
//            false
//
//    }

    fun predict(m: Mat): String {
//        if (!canPredict())
//            return ""
        val n = IntArray(1)
        val p = DoubleArray(1)

        faceRecognizer.predict(m, n, p)
        Log.d(TAG, "array [N] : "+ Arrays.toString(n))
        Log.d(TAG, "array [P]: "+ Arrays.toString(p))
        if (n[0] != -1)
            prob = p[0].toInt()
        else
            prob = -1
        //	if ((n[0] != -1)&&(p[0]<95))
        if (n[0] != -1){
            val idxValue :Int?= labels?.get(n[0])
            return labelsFile!!.get(idxValue!!)
        } else {
            return "Unknow"
        }
    }


    internal fun MatToIplImage(m: org.opencv.core.Mat, width: Int, heigth: Int): IplImage {
        val bmp = Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888)

        Utils.matToBitmap(m, bmp)
        return BitmapToIplImage(bmp, width, heigth)

    }

    internal fun BitmapToIplImage(bmp: Bitmap, width: Int, height: Int): IplImage {
        var bmp = bmp

        if (width != -1 || height != -1) {
            val bmp2 = Bitmap.createScaledBitmap(bmp, width, height, false)
            bmp = bmp2
        }

        val image = IplImage.create(bmp.width, bmp.height,
                IPL_DEPTH_8U, 4)

        bmp.copyPixelsToBuffer(image.byteBuffer)

        val grayImg = IplImage.create(image.width(), image.height(),
                IPL_DEPTH_8U, 1)

        cvCvtColor(image, grayImg, opencv_imgproc.CV_BGR2GRAY)

        return grayImg
    }


    protected fun SaveBmp(bmp: Bitmap, path: String) {
        val file: FileOutputStream
        try {
            file = FileOutputStream(path, true)

            bmp.compress(Bitmap.CompressFormat.JPEG, 100, file)
            file.close()
        } catch (e: Exception) {
            Log.e("", e.message + e.cause)
            e.printStackTrace()
        }

    }

    fun load() {
        train()
    }

    companion object {
        private val TAG = "CtrRecognizer::Class"
        internal val WIDTH = 128
        internal val HEIGHT = 128
        var labels: IntArray? = null
        var mPath: String = Environment.getExternalStorageDirectory().absolutePath + "/ .gms.absen/"
    }
}
