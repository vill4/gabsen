package com.gabsen.ngawi.util

import android.util.Log

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.ArrayList
import java.util.StringTokenizer

class Labels(internal var mPath: String) {

    internal var thelist = ArrayList<label>()
    internal val nmFile:String = "faces.txt"
    val isEmpty: Boolean
        get() = thelist.size <= 0

    internal inner class label(var thelabel: String, var num: Int)

    fun add(s: String, n: Int) {
        thelist.add(label(s, n))
    }

    operator fun get(i: Int): String {
        val Ilabel = thelist.iterator()
        while (Ilabel.hasNext()) {
            val l = Ilabel.next()
            if (l.num == i)
                return l.thelabel
        }
        return ""
    }

    operator fun get(s: String): Int {
        val Ilabel = thelist.iterator()
        while (Ilabel.hasNext()) {
            val l = Ilabel.next()
            if (l.thelabel.equals(s, ignoreCase = true))
                return l.num
        }
        return -1
    }

    fun Save() {
        try {

            val f = File(mPath +nmFile)
            f.createNewFile()
            val bw = BufferedWriter(FileWriter(f))
            val Ilabel = thelist.iterator()
            while (Ilabel.hasNext()) {
                val l = Ilabel.next()
                bw.write(l.thelabel + "," + l.num)
                bw.newLine()
            }
            bw.close()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            Log.e("error", e.message + " " + e.cause)
            e.printStackTrace()
        }

    }

    fun Read() {
        try {
            val fileName= mPath + nmFile
            val lines =  File(fileName).bufferedReader().readLines()

            thelist = ArrayList()
            for (str in lines){
                val tokens = StringTokenizer(str, ",")
                val s = tokens.nextToken()
                val sn = tokens.nextToken()
                thelist.add(label(s, Integer.parseInt(sn)))
            }
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

    }

    fun max(): Int {
        var m = 0
        val Ilabel = thelist.iterator()
        while (Ilabel.hasNext()) {
            val l = Ilabel.next()
            if (l.num > m) m = l.num
        }
        return m
    }
}