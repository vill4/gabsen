package com.gabsen.ngawi.models


data class LoginModel(
        val status:Boolean,
        val message:String,
        val data:LoginDataModel)

data class LoginDataModel(val id:String, val nama:String, val nip: String, val email:String, val username:String,val facemap:String, val token:String)