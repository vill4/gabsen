package com.gabsen.ngawi.models

data class HomeModel(
        val status:Boolean,
        val message:String,
        val code:Int,
        val data:CekAbsenDataModel)

data class CekAbsenDataModel(val jam_masuk:String, val jam_pulang:String, val tanggal:String, val jadwal_masuk:String,
                             val jadwal_pulang:String, val batas_cekin:String, val min_cekout:String)

data class ChartModel(
        val status:Boolean,
        val message:String,
        val code:Int,
        val data:ChartIndexModel)

data class ChartIndexModel(val hadir:List<ChartDataModel>, val ontime:List<ChartDataModel>)

data class ChartDataModel(val key:String, val value: String)

data class AgendaModel(
        val status:Boolean,
        val message:String,
        val code:Int,
        val data:List<AgendaDataModel>)

data class AgendaDataModel(val nama:String, val lokasi:String,val tanggal: String)