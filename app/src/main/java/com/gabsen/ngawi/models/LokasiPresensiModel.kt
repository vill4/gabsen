package com.gabsen.ngawi.models

data class LokasiFetchModel(
        val status: Boolean,
        val message: String,
        val code: Int,
        val data: List<LokasiPresensiModel>)

data class LokasiPresensiModel(var nama: String, var lat: String, var long: String, var radius:String)
