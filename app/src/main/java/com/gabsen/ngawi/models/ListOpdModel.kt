package com.gabsen.ngawi.models

data class ListOpdFetch(
        val status: Boolean,
        val message: String,
        val code: Int,
        val data: List<LokasiPresensiModel>)

class ListOpdModel(var id: String, var nama: String)