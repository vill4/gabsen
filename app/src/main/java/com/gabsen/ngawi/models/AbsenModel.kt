package com.gabsen.ngawi.models

data class AbsenModel(
        val status:Boolean,
        val message:String,
        val code:Int,
        val data:AbsenDataModel)

data class AbsenDataModel(val tanggal:String, val jam:String, val hari:String, val timestamp:String, val lokasi:String)