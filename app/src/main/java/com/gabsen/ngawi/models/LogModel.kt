package com.gabsen.ngawi.models

/**
 * Created by ASUS-PC on 06/10/2017.
 */

class LogModel(var log_id: String, var keterangan: String, var jam: String, var tgl: String, var jenis: String)

data class LogModelFetch(
        val status:Boolean,
        val message:String,
        val code:Int,
        val data:List<LogDataModel>)

data class LogDataModel(val jenis:String, val tanggal:String, val waktu: String, val lokasi:String)